# Blocked, a block editor

Warning: this is completely experimental, not even alpha.

## What is it?

- note-taking
- personal knowledge base
- team collaboration
- personal daily journal

## Brainstorming ideas

- manage text and structured data
- handle data capture and organization
- keyboard and mouse driven
- local-first, offline-first
  - no server to connect, no initial data to load
- collaborative editing
- block-based, all blocks are plugins
- many types of blocks: assets (image), layout (columns, collapse), embed ("iframe"), data
- [renderless](https://adamwathan.me/renderless-components-in-vuejs/)
- every block has its own UUID (and URL as a consequence)
- access data (blocks, databases) from API
- be a PWA
  - seamless integration with mobile APIs
    - contact API
    - calendar API?
- publish a page to a public URL
- allow many block managers: page based, graph, infinite tree
- assets are stored elsewhere (e.g. IPFS, Dat, etc.)
- input rules (like https://prosemirror.net/docs/ref/#inputrules) to have Markdown-like keyboard shortcuts
- add a link to external calendar events, to the block
- keep history of everything, in a history tree
- fork others pages
- templates: transform a page into a template, create a new page from a template
- handle pasting rich content (like a vcalendar event, a vcard, an image, HTML, etc... to be explored...)
- allow every block to be tagged
- implcit database: allow to query blocks of a page, or all pages, with a certain type
- rich data types: phone number, geo location
- start simple, then add complexity: for example at first a to-do list item could be a checkbox with a label, then become a card of a kanban board with 2 columns (false = "to-do", true = "done"), then one could add a new column "doing" having 3 states
- blocks are isolated: each one does not have to know the others
- blocks share a common data model: scalar types, records, ... (to be defined...)
- blocks can be composed of other blocks: for example a bulleted list item is just a paragraph block with a bullet
- blocks have general settings that add items (under a tab?) to application settings, and settings specific to a block instance
- blocks define toolbar items

### Paragraph block

- input rules:
  - recognise some unicode symbols (`=>` become an arrow...)
  - some characters like `(` or `"` insert automatically wrap the selection with opening and closing characters (could be a setting)
- toolbar items: bold, italic, ... (to be defined)

### Bulleted list block

- input rules: `-` or `*`

### To-do list block

- input rules: `(-*)?[ ?]`
- commands
  - check/uncheck box: ctrl+enter

### Image block

- allow to rotate image

## Documentation

- see [ARCHITECTURE.md](./ARCHITECTURE.md)
- see [FAQ.md](./FAQ.md)

## Inspiration

- https://editorjs.io/
- https://www.notion.so/
- https://workflowy.com/
- https://zim-wiki.org/

## Development

### Install

```bash
git clone https://gitlab.com/cbenz/blocked.git
cd blocked
npm install
```

### Run

```bash
npm run dev
```

Open http://localhost:5000 in your browser.

## Building and running in production mode

To create an optimised version of the app:

```bash
npm run build
```

You can run the newly built app with `npm start`. This uses [sirv](https://github.com/lukeed/sirv).
