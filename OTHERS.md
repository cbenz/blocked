# Other editors

## Portable text

cf https://github.com/portabletext/portabletext/blob/master/examples/withLink.json

```json
[
	{
		"_type": "block",
		"_key": "3628734dd519",
		"style": "normal",
		"markDefs": [
			{
				"_type": "link",
				"_key": "e556761904ba",
				"href": "https://www.portabletext.org"
			}
		],
		"children": [
			{
				"_type": "span",
				"_key": "3628734dd5190",
				"text": "This is a paragraph with a ",
				"marks": []
			},
			{
				"_type": "span",
				"_key": "3628734dd5191",
				"text": "link",
				"marks": ["e556761904ba"]
			},
			{
				"_type": "span",
				"_key": "3628734dd5192",
				"text": ".",
				"marks": []
			}
		]
	}
]
```

## Editor.js

Does not abstract inline styles and links, keeps them in HTML.

```json
{
	"time": 1587826086125,
	"blocks": [
		{
			"type": "paragraph",
			"data": {
				"text": "i'm <b>bold</b>&nbsp;and <i>italic</i> and <i>both</i> and a <a href=\"https://news.ycombinator.com/\"><b>super</b> link</a>"
			}
		}
	],
	"version": "2.17.0"
}
```

## Draft JS

using demo at https://react-rte.org/demo or https://draft-wysiwyg.herokuapp.com/

```json
{
	"entityMap": {
		"0": {
			"type": "link",
			"mutability": "MUTABLE",
			"data": {
				"href": "https://news.ycombinator.com"
			}
		}
	},
	"blocks": [
		{
			"key": "1ahm2",
			"text": "now read hacker news!",
			"type": "unstyled",
			"depth": 0,
			"inlineStyleRanges": [{ "offset": 9, "length": 6, "style": "BOLD" }],
			"entityRanges": [{ "offset": 9, "length": 11, "key": 0 }]
		}
	]
}
```

## Quill JS

demo: https://codesandbox.io/s/n93qj4zkwl?file=/src/Editor.js

(not block oriented)

```json
{
	"ops": [
		{ "insert": "now read " },
		{
			"attributes": { "bold": true, "link": "https://news.ycombinator.com" },
			"insert": "hacker"
		},
		{
			"attributes": { "link": "https://news.ycombinator.com" },
			"insert": " news"
		},
		{ "insert": "!\nthat's a new block\n" }
	]
}
```

```json
{
	"ops": [
		{ "insert": "salut\nune liste" },
		{ "attributes": { "list": "bullet" }, "insert": "\n" },
		{ "insert": "une autre" },
		{ "attributes": { "list": "bullet" }, "insert": "\n" }
	]
}
```

## Prosemirror (and tiptap)

Hierarchical data model

```json
{
	"type": "doc",
	"content": [
		{
			"type": "paragraph",
			"content": [
				{
					"type": "text",
					"text": "hi i'm a link"
				}
			]
		},
		{
			"type": "bullet_list",
			"content": [
				{
					"type": "list_item",
					"content": [
						{
							"type": "paragraph",
							"content": [
								{
									"type": "text",
									"text": "list item 1"
								}
							]
						},
						{
							"type": "bullet_list",
							"content": [
								{
									"type": "list_item",
									"content": [
										{
											"type": "paragraph",
											"content": [
												{
													"type": "text",
													"text": "sub item"
												}
											]
										}
									]
								}
							]
						}
					]
				}
			]
		}
	]
}
```
