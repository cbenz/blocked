# Architecture of Blocked

```
JSON <-> model <-> blocked (Svelte editor) <-> unblok (note taking app)
               <-> HTML static renderer
```

## Technical stack

- Svelte
- GunDB
- immer?
- xstate?

## Components

### Block

- defines a BLOCK_TYPE and a BLOCK_API_VERSION
- has a list of inputs
- handles keyboard shortcuts
- defines a data model to represent the caret position inside the block
- defines methods to get and set caret position
- has a paste handler (e.g. paste tweet link)
- defines toolbar actions
- defines settings
- defines methods to load and save state

### Blocks

- handles focusing next / previous block
- handles switching between block and cross-block selection modes
- handles adding, moving, deleting, duplicating a block
- handles displaying the toolbar
- defines methods to load and save state

### Page

- has a title
- has an icon
- has a cover
- has a discussion
- handles keyboard shortcuts (e.g. undo/redo)
- defines methods to load and save state
