import { BlockCommand } from "./blocks/state/blocks"
import isHotkey from "is-hotkey"

type Command = string | (() => BlockCommand)

interface KeyBinding {
	key: string
	when: () => boolean
	command: Command
}

function normalizeCommand(command: Command): BlockCommand {
	return typeof command === "string" ? { type: command } : command()
}

export function matchCommand(
	keyBindings: KeyBinding[],
	event: KeyboardEvent
): BlockCommand {
	for (const { key, when, command } of keyBindings) {
		if (isHotkey(key, event) && (!when || when())) {
			return normalizeCommand(command)
		}
	}
}
