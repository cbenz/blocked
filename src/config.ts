import defaultBlockPlugins, { BlockPlugin } from "./plugins/blocks/index"
import defaultEntityPlugins, { EntityPlugin } from "./plugins/entities/index"
import defaultInlineStylePlugins, {
	InlineStylePlugin,
} from "./plugins/inline-styles.js"

import { PluginRegistry } from "./plugin-registry"

export class Config {
	blockPlugins: PluginRegistry<BlockPlugin>
	defaultBlockType: string
	entityPlugins: PluginRegistry<EntityPlugin>
	inlineStylePlugins: PluginRegistry<InlineStylePlugin>

	constructor({
		blockPlugins,
		entityPlugins,
		inlineStylePlugins,
		defaultBlockType,
	}) {
		this.checkBlockPlugins(blockPlugins)
		this.blockPlugins = blockPlugins

		this.checkDefaultBlockType(defaultBlockType, blockPlugins)
		this.defaultBlockType = defaultBlockType

		this.checkEntityPlugins(entityPlugins)
		this.entityPlugins = entityPlugins

		this.checkInlineStylePlugins(inlineStylePlugins)
		this.inlineStylePlugins = inlineStylePlugins
	}

	checkBlockPlugins(blockPlugins) {
		// TODO
	}

	checkDefaultBlockType(defaultBlockType, blockPlugins) {
		blockPlugins.getByType(defaultBlockType) // will throw an error if not found
	}

	checkEntityPlugins(entityPlugins) {
		// TODO
	}

	checkInlineStylePlugins(inlineStylePlugins) {
		// TODO
	}
}

export const defaultConfig = new Config({
	blockPlugins: defaultBlockPlugins,
	entityPlugins: defaultEntityPlugins,
	inlineStylePlugins: defaultInlineStylePlugins,
	defaultBlockType: "paragraph",
})
