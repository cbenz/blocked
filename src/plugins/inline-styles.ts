import { Plugin, PluginRegistry } from "../plugin-registry"

const plugins = [
	{ type: "BOLD", classes: ["font-bold"] },
	{ type: "ITALIC", classes: ["italic"] },
	{ type: "CODE", classes: ["font-mono"] },
	{ type: "STRIKETHROUGH", classes: ["line-through"] },
]
const defaultInlineStylePlugins = new PluginRegistry("inline-style", plugins)

export interface InlineStylePlugin extends Plugin {
	classes: string[]
}

export default defaultInlineStylePlugins
