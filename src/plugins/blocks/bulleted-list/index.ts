import BulletedListBlock from "./BulletedListBlock.svelte"
import { RichTextBlockState } from "../../../blocks/state/rich-text-block"

export default {
	type: "bulleted-list",
	component: BulletedListBlock,
	state: RichTextBlockState,
}
