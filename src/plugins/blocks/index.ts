import { Plugin, PluginRegistry } from "../../plugin-registry"

import { BlockState } from "../../blocks/index"
import { SvelteComponent } from "svelte"
import bulletedList from "./bulleted-list/index"
import paragraph from "./paragraph/index"

const plugins = [bulletedList, paragraph]
const defaultBlockPlugins = new PluginRegistry("block", plugins)

export interface BlockPlugin extends Plugin {
	component: SvelteComponent
	state: typeof BlockState
}

export default defaultBlockPlugins
