import ParagraphBlock from "./ParagraphBlock.svelte"
import { RichTextBlockState } from "../../../blocks/state/rich-text-block"

export default {
	type: "paragraph",
	component: ParagraphBlock,
	state: RichTextBlockState,
}

// TODO
// 	handleAction(type, payload) {
// 		switch (type) {
// 			case "split": {
// 				const { range, ref } = payload

// 				throw new Error("TODO split")
// 				// // Pre-caret
// 				// const preCaretRange = new Range()
// 				// preCaretRange.selectNodeContents(ref)
// 				// preCaretRange.setEnd(range.startContainer, range.startOffset)
// 				// this.data = getRangeHTML(preCaretRange)

// 				// // Post-caret
// 				// const postCaretRange = new Range()
// 				// postCaretRange.selectNodeContents(ref)
// 				// postCaretRange.setStart(range.endContainer, range.endOffset)
// 				// const postCaretHTML = getRangeHTML(postCaretRange)

// 				// return {
// 				// 	postProcess: {
// 				// 		name: "insertBlockAfter",
// 				// 		value: this.clone({ data: postCaretHTML }),
// 				// 	},
// 				// 	effect: { type: "focusNextBlock" },
// 				// }
// 			}
// 			default:
// 				throw new Error(
// 					`Invalid action type "${type}" for ${this.constructor.name}`
// 				)
// 		}
// 	}
