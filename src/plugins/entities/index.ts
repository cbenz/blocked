import { Plugin, PluginRegistry } from "../../plugin-registry"

import { SvelteComponent } from "svelte"
import link from "./link/index"

const plugins = [link]
const defaultEntityPlugins = new PluginRegistry("entity", plugins)

export interface EntityPlugin extends Plugin {
	component: SvelteComponent
	// state: typeof EntityState
}

export default defaultEntityPlugins
