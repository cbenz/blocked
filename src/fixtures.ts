export const example1 = {
	blocks: [
		{ type: "paragraph", text: "Hi I'm a text block!" },
		{
			type: "paragraph",
			text: "There's bold, italic and both!",
			inlineStyleRanges: [
				{ offset: 8, length: 4, style: "BOLD" },
				{ offset: 14, length: 6, style: "ITALIC" },
				{ offset: 25, length: 4, style: "BOLD" },
				{ offset: 25, length: 4, style: "ITALIC" },
			],
		},
		{
			type: "paragraph",
			text: "Also available: code and strike-through.",
			inlineStyleRanges: [
				{ offset: 16, length: 4, style: "CODE" },
				{ offset: 25, length: 14, style: "STRIKETHROUGH" },
			],
		},
		{
			type: "paragraph",
			text: "Now go and read hacker news.",
			inlineStyleRanges: [{ offset: 16, length: 6, style: "BOLD" }],
			entityRanges: [{ offset: 16, length: 11, key: "0" }],
		},
		{
			type: "bulleted-list",
			depth: 0,
			text: "I'm a list item",
		},
		{
			type: "bulleted-list",
			depth: 1,
			text: "I'm a list item of depth 1!",
		},
	],
	entityMap: {
		"0": {
			type: "LINK",
			data: { url: "https://news.ycombinator.com" },
		},
	},
}

// export const example1 = [
// 	{
// 		type: "paragraph",
// 		inlineItems: [{ text: "Hi I'm a text block!" }],
// 	},
// 	{
// 		type: "paragraph",
// 		inlineItems: [
// 			{ text: "There's " },
// 			{ text: "bold", marks: ["bold"] },
// 			{ text: ", " },
// 			{ text: "italic", marks: ["italic"] },
// 			{ text: " and " },
// 			{ text: "both", marks: ["bold", "italic"] },
// 			{ text: "! Also available: " },
// 			{ text: "code", marks: ["code"] },
// 			{ text: " and " },
// 			{ text: "strike-through", marks: ["strikeThrough"] },
// 			{ text: ". Now go and read " },
// 			{
// 				type: "link",
// 				inlineItems: [
// 					{ text: "hacker", marks: ["text-red-500"] }, //
// 					{ text: " news" },
// 				],
// 				href: "https://news.ycombinator.com/",
// 				external: true,
// 			},
// 			{ text: ". This inline item could be " },
// 			{ text: "merged with this one." },
// 		],
// 	},
// 	{
// 		type: "bulleted-list",
// 		depth: 0,
// 		inlineItems: [{ text: "I'm a list item" }],
// 	},
// 	{
// 		type: "bulleted-list",
// 		depth: 1,
// 		inlineItems: [{ text: "I'm a list item of depth 1!" }],
// 	},
// ]
