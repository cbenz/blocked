export function findTextNodesUnder(element: Element) {
	let node
	const nodes = []

	const nodeIterator = element.ownerDocument.createNodeIterator(
		element,
		NodeFilter.SHOW_TEXT,
		{
			acceptNode(node) {
				return node.textContent.length > 0
					? NodeFilter.FILTER_ACCEPT
					: NodeFilter.FILTER_REJECT
			},
		}
	)

	while ((node = nodeIterator.nextNode())) {
		nodes.push(node)
	}

	return nodes
}
