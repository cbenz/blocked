const EVENT_NAME = "selectionchange"

export default function selectionchange(element: Element) {
	const document = element.ownerDocument

	function handleSelectionChange() {
		const selection = document.getSelection()
		if (element.contains(selection.focusNode)) {
			element.dispatchEvent(new CustomEvent(EVENT_NAME, { detail: selection }))
		}
	}

	document.addEventListener(EVENT_NAME, handleSelectionChange)

	return {
		destroy() {
			document.removeEventListener(EVENT_NAME, handleSelectionChange)
		},
	}
}
