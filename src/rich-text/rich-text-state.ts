import { RichText } from "./index"
import _ from "lodash"

export class RichTextState {
	text: string
	entityRanges: RichTextRange[]
	inlineStyleRanges: RichTextRange[]

	constructor(
		text: string = "",
		entityRanges: EntityRange[] = [],
		inlineStyleRanges: InlineStyleRange[] = []
	) {
		this.text = text

		this.checkEntityRanges(entityRanges)
		this.entityRanges = entityRanges

		this.checkInlineStyleRanges(inlineStyleRanges)
		this.inlineStyleRanges = inlineStyleRanges
	}

	static fromJSON({
		text,
		inlineStyleRanges,
		entityRanges,
	}: {
		text: string
		inlineStyleRanges?: InlineStyleRangeJSON[]
		entityRanges?: EntityRangeJSON[]
	}) {
		return new RichTextState(
			text,
			(entityRanges || []).map(EntityRange.fromJSON),
			(inlineStyleRanges || []).map(InlineStyleRange.fromJSON)
		)
	}

	toJSON(): RichTextStateJSON {
		const { text } = this
		let result: RichTextStateJSON = { text }
		if (this.entityRanges.length > 0) {
			result.entityRanges = this.entityRanges.map((range) => range.toJSON())
		}
		if (this.inlineStyleRanges.length > 0) {
			result.inlineStyleRanges = this.inlineStyleRanges.map((range) =>
				range.toJSON()
			)
		}
		return result
	}

	checkInlineStyleRange(inlineStyleRange: InlineStyleRange) {
		// TODO check that style exists
	}

	checkInlineStyleRanges(inlineStyleRanges: InlineStyleRange[]) {
		for (const inlineStyleRange of inlineStyleRanges) {
			this.checkInlineStyleRange(inlineStyleRange)
		}
	}

	checkEntityRange(entityRange: EntityRange) {
		// TODO
		// - check offset and length are valid
		// - entity exists
	}

	checkEntityRanges(entityRanges: EntityRange[]) {
		for (const entityRange of entityRanges) {
			this.checkEntityRange(entityRange)
		}
	}

	isCaretCollapsedAtStart(selection) {
		return selection.length === 0 && selection.offset === 0
	}

	isCaretCollapsedAtEnd(selection) {
		return selection.length === 0 && selection.offset === this.text.length
	}

	deleteText(selection: RichTextRange) {
		// Update text
		const { text } = this
		const newText = [
			text.slice(0, selection.offset),
			text.slice(selection.endOffset),
		].join("")
		this.text = newText

		// Update ranges
		this.inlineStyleRanges = this.processRanges(selection, (range) =>
			deleteInRange(selection, range)
		)

		// Update selection
		selection.length = 0
	}

	deleteTextBackward(selection: RichTextRange) {
		if (selection.length === 0 && selection.offset > 0) {
			selection.growLeft(1)
		}
		this.deleteText(selection)
	}

	deleteTextForward(selection: RichTextRange) {
		if (selection.length === 0 && selection.offset < this.text.length) {
			selection.growRight(1)
		}
		this.deleteText(selection)
	}

	insertText(selection: RichTextRange, insertedText: string = "") {
		// If selection is not collapsed, delete selected text.
		if (selection.length > 0) {
			this.deleteText(selection)
		}

		const { offset } = selection

		// Update text
		const { text } = this
		const newText = [
			text.slice(0, offset),
			insertedText,
			text.slice(offset),
		].join("")
		this.text = newText

		// Update ranges
		this.inlineStyleRanges = this.processRanges(selection, (range) =>
			insertInRange(offset, insertedText.length, range)
		)

		// Update selection
		selection.moveRight(insertedText.length)
	}

	private processRanges(selection: RichTextRange, processRange) {
		const newRanges = []
		for (const range of this.inlineStyleRanges) {
			if (
				selection.offset <= range.offset &&
				selection.endOffset >= range.endOffset
			) {
				// When selection contains the whole range, the range disappears.
				continue
			}
			const newRange = processRange(range)
			newRanges.push(newRange)
		}
		return newRanges
	}
}

export function computeInlineItems(state: RichTextState) {
	const { text, inlineStyleRanges } = state
	const inlineItems = computeSetList(
		inlineStyleRanges,
		text.length - 1,
		"style"
	)
		.reduce((acc, current, index) => {
			const newInlineItem = {
				offset: index,
				length: 1,
				styles: current,
			}
			if (acc.length === 0) {
				acc.push(newInlineItem)
			} else {
				const lastInlineItem = acc[acc.length - 1]
				if (_.isEqual(current, lastInlineItem.styles)) {
					lastInlineItem.length++
				} else {
					acc.push(newInlineItem)
				}
			}
			return acc
		}, [])
		.map(({ offset, length, styles }) => ({
			styles: Array.from(styles),
			text: text.slice(offset, offset + length),
		}))
	return inlineItems
}

function computeSetList(
	ranges: RichTextRange[],
	endOffset: number,
	rangeDataProperty: string
) {
	const setList = Array(endOffset + 1)
		.fill(null)
		.map(() => new Set())
	ranges.forEach((range) => {
		let cursor = range.offset
		const end = cursor + range.length
		while (cursor < end) {
			setList[cursor] = setList[cursor].add(range[rangeDataProperty])
			cursor++
		}
	})
	return setList
}

export function deleteInRange(selection: RichTextRange, range: RichTextRange) {
	if (selection.length === 0 || selection.offset >= range.endOffset) {
		// When selection is collapsed, or is is after range, the range is kept as-is.
		return range
	}
	const result = range.clone()
	if (selection.offset <= range.offset && selection.endOffset <= range.offset) {
		// When selection is before range (touching or not), the range is moved left.
		result.moveLeft(selection.length)
		return result
	} else if (
		selection.offset <= range.offset &&
		selection.endOffset > range.offset &&
		selection.endOffset < range.endOffset
	) {
		// When selection intersects range from the left, the range is moved left and is shortened by the length of the intersection.
		const intersection = new RichTextRange(
			range.offset,
			selection.endOffset - range.offset
		)
		const rest = new RichTextRange(
			selection.offset,
			range.offset - selection.offset
		)
		result.length -= intersection.length
		result.moveLeft(rest.length)
		return result
	} else if (
		selection.offset <= range.offset &&
		selection.endOffset >= range.endOffset
	) {
		throw new Error(
			"Could not delete selection in range because selection contains range: operation has to be done at a higher level."
		)
	} else if (
		selection.offset >= range.offset &&
		selection.offset <= range.endOffset &&
		selection.endOffset >= range.offset &&
		selection.endOffset <= range.endOffset
	) {
		// When selection is inside range (touching 0 or 1 of the edges), the range is shortened by the length of the selection.
		// The case when selection touches both edges is eliminated by the previous "if".
		result.length -= selection.length
		return result
	} else if (
		selection.offset >= range.offset &&
		selection.offset < range.endOffset &&
		selection.endOffset > range.endOffset
	) {
		// When selection intersects range from the right, the range is shortened by the length of the intersection.
		const intersection = new RichTextRange(
			selection.offset,
			range.endOffset - selection.offset
		)
		result.length -= intersection.length
		return result
	}
}

export function insertInRange(
	offset: number,
	length: number,
	range: RichTextRange
) {
	const result = range.clone()
	if (offset <= range.offset) {
		result.moveRight(length)
	} else if (offset > range.offset && offset <= range.endOffset) {
		result.length += length
	}
	return result
}

export class RichTextRange {
	offset: number
	length: number

	constructor(offset: number, length: number = 0) {
		this.offset = offset
		if (length < 0) {
			throw new Error(`Invalid length [${offset}] for range`)
		}
		this.length = length
	}

	toJSON(): RichTextRangeJSON {
		const { offset, length } = this
		return { offset, length }
	}

	get endOffset() {
		return this.offset + this.length
	}

	clone() {
		return new RichTextRange(this.offset, this.length)
	}

	growLeft(length: number) {
		if (length < 0) {
			throw new Error(
				`Could not grow range with a negative length, use growRight`
			)
		}
		const newOffset = this.offset - length
		if (newOffset < 0) {
			throw new Error(`Could not grow range to left below offset 0`)
		}
		this.offset = newOffset
		this.length += length
	}

	growRight(length: number) {
		if (length < 0) {
			throw new Error(
				`Could not grow range with a negative length, use growLeft`
			)
		}
		this.length += length
	}

	moveLeft(offset: number) {
		if (offset < 0) {
			throw new Error(
				`Could not move range with a negative offset, use moveRight`
			)
		}
		const newOffset = this.offset - offset
		if (newOffset < 0) {
			throw new Error(`Could not move range to left below offset 0`)
		}
		this.offset = newOffset
	}

	moveRight(offset: number) {
		if (offset < 0) {
			throw new Error(
				`Could not move range with a negative offset, use moveLeft`
			)
		}
		this.offset += offset
	}

	// intersectsWith(other: RichTextRange) {
	// 	return other.endOffset >= this.offset && other.offset <= this.endOffset
	// }

	// intersection(other: RichTextRange) {
	// 	let min, max
	// 	if (this.offset < other.offset) {
	// 		min = this
	// 		max = other
	// 	} else {
	// 		min = other
	// 		max = this
	// 	}
	// 	if (min.endOffset < max.offset) {
	// 		return null
	// 	}
	// 	return new RichTextRange(
	// 		max.offset,
	// 			(min.endOffset < max.endOffset ? min.endOffset : max.endOffset) -
	// 			max.offset
	// 	)
	// }

	// split(at) {
	// 	const { offset, length } = this
	// 	return [
	// 		new this.constructor({ offset, length: at - offset }),
	// 		new this.constructor({ offset: at, length: length - at }),
	// 	]
	// }

	// delete(selection: RichTextRange) {
	// 		// Selection is after range: the range is kept as-is.
	// 		if (selection.offset <= this.endOffset) {
	//       return
	//     }

	// }

	// insert(offset: number, length: number) {
	// 	//
	// }

	// update(selection: RichTextRange, insertedTextLength: number) {
	// 	if (selection.offset <= this.offset) {
	// 		const intersectionLength = selection.endOffset - this.offset
	// 		if (intersectionLength > 0 && selection.offset <= this.offset) {
	// 			this.offset = selection.offset + insertedTextLength
	// 			this.length -= intersectionLength
	// 		} else {
	// 			this.offset += insertedTextLength - selection.length
	// 		}
	// 	} else if (
	// 		selection.offset > this.offset &&
	// 		selection.offset <= this.endOffset
	// 	) {
	// 		const intersectionLength =
	// 			Math.min(this.endOffset, selection.endOffset) - selection.offset
	// 		this.length += insertedTextLength
	// 		if (intersectionLength > 0) {
	// 			this.length -= intersectionLength
	// 		}
	// 	}
	// }
}

export class EntityRange extends RichTextRange {
	key: string

	constructor(offset: number, length: number, key: string) {
		super(offset, length)
		this.checkKey(key)
		this.key = key
	}

	static fromJSON(data: EntityRangeJSON) {
		const { offset, length, key } = data
		return new EntityRange(offset, length, key)
	}

	toJSON(): EntityRangeJSON {
		const { key } = this
		return { ...super.toJSON(), key }
	}

	clone() {
		return new EntityRange(this.offset, this.length, this.key)
	}

	checkKey(key: string) {
		// TODO
	}

	// split(at) {
	// 	const { key } = this
	// 	return super.split(at).map((range) => null)
	// 	debugger
	// 	return [
	// 		new this.constructor({ offset, length: at - offset, key }),
	// 		new this.constructor({ offset: at, length: length - at, key }),
	// 	]
	// }
}

export class InlineStyleRange extends RichTextRange {
	style: string

	constructor(offset: number, length: number, style: string) {
		super(offset, length)
		this.checkStyle(style)
		this.style = style
	}

	static fromJSON(data: InlineStyleRangeJSON) {
		const { offset, length, style } = data
		return new InlineStyleRange(offset, length, style)
	}

	toJSON(): InlineStyleRangeJSON {
		const { style } = this
		return { ...super.toJSON(), style }
	}

	clone() {
		return new InlineStyleRange(this.offset, this.length, this.style)
	}

	checkStyle(style: string) {
		// TODO
	}

	// split(at) {
	// 	const { style } = this
	// 	return super.split(at).map((range) => null)
	// 	debugger
	// 	return [
	// 		new this.constructor({ offset, length: at - offset, style }),
	// 		new this.constructor({ offset: at, length: length - at, style }),
	// 	]
	// }
}

interface RichTextStateJSON {
	text: string
	entityRanges?: RichTextRangeJSON[]
	inlineStyleRanges?: RichTextRangeJSON[]
}

interface RichTextRangeJSON {
	offset: number
	length: number
}

interface EntityRangeJSON extends RichTextRangeJSON {
	key: string
}

interface InlineStyleRangeJSON extends RichTextRangeJSON {
	style: string
}
