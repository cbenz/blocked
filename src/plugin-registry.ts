export interface Plugin {
	type: string
}

type PluginMap<T extends Plugin> = Map<string, T>

export class PluginRegistry<T extends Plugin> {
	type: string
	plugins: PluginMap<T>

	constructor(type: string, plugins: T[]) {
		this.type = type
		// TODO check plugins
		this.plugins = new Map(plugins.map((plugin) => [plugin.type, plugin]))
	}

	getByType(type: string) {
		const plugin = this.plugins.get(type)
		if (!plugin) {
			throw new Error(`Could not find ${this.type} plugin for type "${type}"`)
		}
		return plugin
	}
}
