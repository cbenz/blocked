import { BlockState, BlockStateJSON } from "./block"
import { EntityState, EntityStateJSON } from "./entity"

import { Config } from "../../config"

export interface BlocksStateJSON {
	blocks: BlockStateJSON[]
	entityMap?: { [key: string]: EntityStateJSON }
}

type EntityMap = Map<string, EntityState>

export interface BlockCommand {
	type: string
	payload?: object
}

export interface BlockEffect {
	type: string
	payload?: object
}

export class BlocksState {
	blocks: BlockState[]
	entityMap: EntityMap
	config: Config

	constructor(blocks: BlockState[], entityMap: EntityMap, config: Config) {
		this.blocks = blocks
		this.entityMap = entityMap
		this.config = config
	}

	static fromJSON(data: BlocksStateJSON, config: Config) {
		const blocks = data.blocks.map((block, blockIndex) =>
			BlockState.fromJSON(block, { blockIndex }, config)
		)
		const entityMap = new Map(
			Object.entries(data.entityMap).map(([entityKey, entityData]) => [
				entityKey,
				EntityState.fromJSON(entityData, config),
			])
		)
		const blocksState = new BlocksState(blocks, entityMap, config)
		// Define parent for each block after blocksState is created.
		for (const block of blocksState.blocks) {
			block.blocksState = blocksState
		}
		return blocksState
	}

	toJSON(): BlocksStateJSON {
		let result: BlocksStateJSON = {
			blocks: this.blocks.map((block) => block.toJSON()),
		}
		if (this.entityMap.size > 0) {
			const entityMapJSON = {}
			for (const [key, entity] of this.entityMap) {
				entityMapJSON[key] = entity.toJSON()
			}
			result.entityMap = entityMapJSON
		}
		return result
	}

	get count() {
		return this.blocks.length
	}

	canExecuteCommand(blockIndex: number, type: string): boolean {
		switch (type) {
			case "create":
				return true
			case "delete":
				return true
			case "swapWithPrevious":
				return !this.blocks[blockIndex].isFirstBlock()
			case "swapWithNext":
				return !this.blocks[blockIndex].isLastBlock()
			default:
				// const block = this.blocks[blockIndex]
				// return block.canExecuteCommand(type)
				throw new Error(
					`Invalid command type "${type}" for ${this.constructor.name}`
				)
		}
	}

	executeCommand(blockIndex: number, command: BlockCommand): BlockEffect[] {
		const { type, payload } = command
		if (!this.canExecuteCommand(blockIndex, type)) {
			return
		}
		switch (type) {
			case "create":
				this.insertBlock(blockIndex + 1, this.config.defaultBlockType)
				return
			case "delete":
				// TODO
				// this.blocks.splice(blockIndex, 1)
				// if (this.blocks.length === 0) {
				// 	this.blocks.push(this.createDefaultBlock())
				// }
				return
			case "swapWithPrevious":
				// TODO
				return
			case "swapWithNext":
				// TODO
				return
			default:
				// const block = this.blocks[blockIndex]
				// return block.executeCommand(command)
				throw new Error(
					`Invalid command type "${type}" for ${this.constructor.name}`
				)
		}
	}

	insertBlock(blockIndex: number, type: string) {
		const blockPlugin = this.config.blockPlugins.getByType(type)
		const newBlock = blockPlugin.state.fromJSON(
			{ type },
			{
				blockIndex,
				blocksState: this,
			},
			this.config
		)
		this.blocks.splice(blockIndex, 0, newBlock)
		for (const block of this.blocks.slice(blockIndex + 1)) {
			block.blockIndex++
		}
	}
}
