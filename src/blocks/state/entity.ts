import { Config } from "../../config"

export interface EntityStateJSON {
	type: string
	data: any
}

export class EntityState {
	type: string
	data: any

	constructor(type: string, data: any, config: Config) {
		// TODO check type
		this.type = type

		// TODO check data
		this.data = data
	}

	static fromJSON(
		{ type, data }: EntityStateJSON,
		config: Config
	): EntityState {
		return new EntityState(type, data, config)
	}

	toJSON(): EntityStateJSON {
		const { type, data } = this
		return { type, data }
	}
}
