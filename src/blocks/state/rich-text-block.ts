import { BlockState } from "./block"
import { BlocksState } from "./blocks"
import { RichTextState } from "../../rich-text/index"

export class RichTextBlockState extends BlockState {
	richText: RichTextState

	constructor(
		type: string,
		blockIndex: number,
		richText: RichTextState,
		options: {
			key?: string
			depth?: number
			blocksState?: BlocksState
		}
	) {
		super(type, blockIndex, options)
		// TODO check richText
		this.richText = richText
	}

	static fromJSON(data, metadata) {
		const { type, key, depth, text, entityRanges, inlineStyleRanges } = data
		const richText = RichTextState.fromJSON({
			text,
			entityRanges,
			inlineStyleRanges,
		})
		const { blockIndex, blocksState } = metadata
		return new RichTextBlockState(type, blockIndex, richText, {
			key,
			depth,
			blocksState,
		})
	}

	toJSON() {
		return {
			...super.toJSON(),
			...this.richText.toJSON(),
		}
	}

	isTextBlock() {
		return true
	}

	canExecuteCommand(type: string): boolean {
		switch (type) {
			case "focusPreviousTextBlock":
				return !!this.previousTextBlock
			case "focusNextTextBlock":
				return !!this.nextTextBlock
			default:
				return super.canExecuteCommand(type)
		}
	}

	executeCommand(command) {
		const { type, payload } = command
		if (!this.canExecuteCommand(type)) {
			return
		}
		switch (type) {
			case "focusPreviousTextBlock":
				return [
					{
						type: "focus",
						blockIndex: this.previousTextBlock.blockIndex,
						payload,
					},
				]
			case "focusNextTextBlock":
				return [
					{
						type: "focus",
						blockIndex: this.nextTextBlock.blockIndex,
						payload,
					},
				]
			default:
				return super.executeCommand(command)
		}
	}
}
