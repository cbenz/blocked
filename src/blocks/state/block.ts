import { BlockCommand, BlockEffect, BlocksState } from "./blocks"

import { nanoid } from "nanoid"

export interface BlockStateJSON {
	type: string
	key: string
	depth: number
}

export class BlockState {
	type: string
	key: string
	depth: number
	blockIndex: number
	blocksState: BlocksState

	constructor(
		type: string,
		blockIndex: number,
		{
			key,
			depth,
			blocksState,
		}: {
			key?: string
			depth?: number
			blocksState?: BlocksState
		}
	) {
		this.type = type
		this.key = key || nanoid()
		this.depth = depth || 0
		this.blockIndex = blockIndex
		this.blocksState = blocksState
	}

	static fromJSON(data, metadata, config) {
		const type = data.type || config.defaultBlockType
		const blockPlugin = config.blockPlugins.getByType(type)
		return blockPlugin.state.fromJSON(data, metadata)
	}

	toJSON(): BlockStateJSON {
		const { type, key, depth } = this
		return { type, key, depth }
	}

	isTextBlock() {
		return false
	}

	isFirstBlock() {
		return this.blockIndex === 0
	}

	isLastBlock() {
		return this.blockIndex === this.blocksState.count - 1
	}

	findPreviousBlock(predicate?: (BlockState) => boolean): BlockState {
		const { blocks } = this.blocksState
		return this.isFirstBlock()
			? null
			: predicate
			? blocks.slice(0, this.blockIndex).reverse().find(predicate)
			: blocks[this.blockIndex - 1]
	}

	findNextBlock(predicate?: (BlockState) => boolean): BlockState {
		const { blocks } = this.blocksState
		return this.isLastBlock()
			? null
			: predicate
			? blocks.slice(this.blockIndex + 1).find(predicate)
			: blocks[this.blockIndex + 1]
	}

	get previousBlock() {
		return this.findPreviousBlock()
	}

	get nextBlock() {
		return this.findNextBlock()
	}

	get previousTextBlock() {
		return this.findPreviousBlock((block) => block.isTextBlock())
	}

	get nextTextBlock() {
		return this.findNextBlock((block) => block.isTextBlock())
	}

	canExecuteCommand(type: string): boolean {
		const { depth } = this
		switch (type) {
			case "dedent":
				return depth > 0
			case "indent": {
				const { previousBlock } = this
				return previousBlock && depth <= previousBlock.depth
			}
			default:
				return this.blocksState.canExecuteCommand(this.blockIndex, type)
		}
	}

	executeCommand(command: BlockCommand): BlockEffect[] {
		const { type, payload } = command
		if (!this.canExecuteCommand(type)) {
			return
		}
		switch (type) {
			case "dedent":
				this.depth--
				return
			case "indent":
				this.depth++
				return
			default:
				return this.blocksState.executeCommand(this.blockIndex, command)
		}
	}
}
