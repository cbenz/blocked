// export class BlockManager {
// 	constructor(blocks) {
// 		this.blocks = blocks
// 		this.selectedBlockIds = new Set()
// 		this.effects = []
// 		if (blocks.length > 0) {
// 			this.effects.push({ type: "focus", blockIndex: 0 })
// 		}
// 	}

// 	static fromJSON(data) {
// 		// TODO Validate using JSON schema
// 		return new BlockManager(data.map(Block.fromJSON))
// 	}

// 	canHandleAction(blockIndex, action) {
// 		const { type, payload } = action

// 		switch (type) {
// 			case "create":
// 				return blockIndex < this.blocks.length
// 			case "delete":
// 				return true
// 			case "focusPrevious":
// 				return true
// 			case "focusNext":
// 				return true
// 			case "swapWithPrevious":
// 				return blockIndex > 0
// 			case "swapWithNext":
// 				return blockIndex < this.blocks.length - 1
// 			case "mergeWithPrevious":
// 				return blockIndex > 0
// 			case "mergeWithNext":
// 				return blockIndex < this.blocks.length - 1
// 			case "dedent":
// 				return this.blocks[blockIndex].depth > 0
// 			case "indent":
// 				return true
// 			case "select":
// 				return true
// 			case "deselect":
// 				return this.selectedBlockIds.size > 0
// 			default:
// 				return this.blocks[blockIndex].canHandleAction(type, payload)
// 		}
// 	}

// 	handleAction(blockIndex, action) {
// 		console.log("BlockManager handleAction", { blockIndex, action })
// 		if (!this.canHandleAction(blockIndex, action)) {
// 			return
// 		}
// 		this._handleBlockListAction(blockIndex, action)
// 	}

// 	_handleBlockAction(blockIndex, action) {
// 		const postProcessors = {
// 			insertBlockAfter: (item) => (blockIndex) => {
// 				this.blocks.splice(blockIndex + 1, 0, item)
// 			},
// 			// replaceBlockByMany: (items) => (blockIndex) => {
// 			// 	this.blocks.splice(blockIndex, 0, items)
// 			// },
// 		}

// 		const block = this.blocks[blockIndex]
// 		const { type, payload } = action
// 		const result = block.handleAction(type, payload)
// 		if (result instanceof Block) {
// 			this.blocks[blockIndex] = result
// 		} else {
// 			const { postProcess } = result
// 			if (postProcess) {
// 				postProcessors[postProcess.name](postProcess.value)(blockIndex)
// 			}

// 			let { effect } = result
// 			if (effect) {
// 				switch (effect.type) {
// 					case "focusNextBlock":
// 						effect.type = "focus"
// 						effect.blockIndex = blockIndex + 1
// 						break
// 				}
// 				if (
// 					effect.blockIndex >= 0 &&
// 					effect.blockIndex <= this.blocks.length - 1
// 				) {
// 					this.effects.push(effect)
// 				}
// 			}
// 		}
// 	}

// 	_handleBlockListAction(blockIndex, action) {
// 		const { type, payload } = action
// 		const block = this.blocks[blockIndex]
// 		switch (type) {
// 			case "create":
// 				this.blocks.splice(blockIndex + 1, 0, Block.createDefaultBlock())
// 				this.effects.push({ type: "focus", blockIndex: blockIndex + 1 })
// 				break
// 			case "delete":
// 				this.blocks.splice(blockIndex, 1)
// 				if (this.blocks.length === 0) {
// 					this.blocks.push(Block.createDefaultBlock())
// 				}
// 				this.effects.push({
// 					type: "focus",
// 					blockIndex:
// 						blockIndex >= this.blocks.length
// 							? this.blocks.length - 1
// 							: blockIndex,
// 				})
// 				break
// 			case "focusPrevious":
// 				this.effects.push({
// 					type: "focus",
// 					blockIndex: blockIndex - 1,
// 					payload,
// 				})
// 				break
// 			case "focusNext":
// 				this.effects.push({
// 					type: "focus",
// 					blockIndex: blockIndex + 1,
// 					payload,
// 				})
// 				break
// 			case "swapWithNext":
// 				this.blocks[blockIndex] = this.blocks[blockIndex + 1]
// 				this.blocks[blockIndex + 1] = block
// 				this.effects.push({ type: "focus", blockIndex: blockIndex + 1 })
// 				break
// 			case "swapWithPrevious":
// 				this.blocks[blockIndex] = this.blocks[blockIndex - 1]
// 				this.blocks[blockIndex - 1] = block
// 				this.effects.push({ type: "focus", blockIndex: blockIndex - 1 })
// 				break
// 			case "mergeWithNext":
// 				console.log("TODO")
// 				// const block = this.blocks[blockIndex]
// 				// this.blocks.splice(blockIndex, 1)
// 				// this.blocks[blockIndex]
// 				break
// 			case "mergeWithPrevious":
// 				console.log("TODO")
// 				break
// 			case "dedent":
// 				this.blocks[blockIndex].depth--
// 				break
// 			case "indent":
// 				this.blocks[blockIndex].depth++
// 				break
// 			case "select":
// 				this.selectedBlockIds.add(this.blocks[blockIndex].id)
// 				break
// 			case "deselect":
// 				this.selectedBlockIds = new Set()
// 				break
// 			default:
// 				this._handleBlockAction(blockIndex, action)
// 		}
// 	}

// 	toJSON() {
// 		return {
// 			blocks: this.blocks.map((block) => block.toJSON()),
// 			effects: this.effects,
// 			selectedBlockIds: [...this.selectedBlockIds],
// 		}
// 	}
// }
