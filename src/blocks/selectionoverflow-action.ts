const EVENT_NAME = "selectionoverflow"

export default function selectionoverflow(element: Element) {
	const document = element.ownerDocument

	// Element that was under pointer when mousedown occurred.
	let mouseDownElement = null

	function handleMouseDown(event: MouseEvent) {
		if (element.contains(event.target as Node)) {
			mouseDownElement = element
		}
	}

	function handleMouseMove(event: MouseEvent) {
		function dispatch(direction) {
			mouseDownElement.dispatchEvent(
				new CustomEvent(EVENT_NAME, {
					detail: { mouseEvent: event, direction },
				})
			)
		}
		if (mouseDownElement === null || event.buttons === 0) {
			return
		}
		const rect = mouseDownElement.getBoundingClientRect()
		if (event.clientY < rect.top) {
			dispatch("up")
		} else if (event.clientY > rect.bottom) {
			dispatch("down")
		}
	}

	function handleMouseUp(event: MouseEvent) {
		mouseDownElement = null
	}

	document.addEventListener("mousedown", handleMouseDown)
	document.addEventListener("mousemove", handleMouseMove)
	document.addEventListener("mouseup", handleMouseUp)

	return {
		destroy() {
			document.removeEventListener("mousedown", handleMouseDown)
			document.removeEventListener("mousemove", handleMouseMove)
			document.removeEventListener("mouseup", handleMouseUp)
		},
	}
}
