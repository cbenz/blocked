const { backgroundColor } = require("tailwindcss/defaultTheme")

module.exports = {
	theme: {
		extend: {
			backgroundColor: () => ({
				...backgroundColor,
				current: "currentColor",
			}),
		},
	},
	variants: {},
	plugins: [],
}
