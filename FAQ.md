# Frequently Asked Questions

## Why not versioning Markdown files with Git?

- Markdown is not conveninent enough to allow editing tables
- Markdown is not WYSIWYG
- Git is not adapted to real-time collaboration

## What app could it replace?

- note-taking apps
- spreadsheet
- shopping list apps
- personal knowledge base
- kanban board
