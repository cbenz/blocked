import {
	RichTextRange,
	RichTextState,
	deleteInRange,
	insertInRange,
} from "../src/rich-text/rich-text-state"

describe("RichTextRange", () => {
	describe("constructor", () => {
		test("should succeed when constructing a range with offset only", () => {
			const range = new RichTextRange(0)
			expect(range.offset).toBe(0)
			expect(range.length).toBe(0)
		})
		test("should fail when constructing a range with a negative length", () => {
			expect(() => {
				new RichTextRange(0, -1)
			}).toThrow()
		})
	})
	describe("endOffset", () => {
		test("should succeed", () => {
			const range = new RichTextRange(2, 1)
			expect(range.endOffset).toBe(3)
		})
	})
	describe("growLeft", () => {
		test("should succeed with a range with offset 2", () => {
			const range = new RichTextRange(2, 1)
			range.growLeft(1)
			expect(range.offset).toBe(1)
			expect(range.length).toBe(2)
		})
		test("should do nothing with a length 0", () => {
			const range = new RichTextRange(2, 1)
			range.growLeft(0)
			expect(range.offset).toBe(2)
			expect(range.length).toBe(1)
		})
		test("should fail with a range with offset 0", () => {
			const range = new RichTextRange(0, 1)
			expect(() => {
				range.growLeft(1)
			}).toThrow()
		})
		test("should fail with a negative length", () => {
			const range = new RichTextRange(0, 1)
			expect(() => {
				range.growLeft(-1)
			}).toThrow()
		})
	})
	describe("growRight", () => {
		test("should succeed", () => {
			const range = new RichTextRange(2, 1)
			range.growRight(1)
			expect(range.offset).toBe(2)
			expect(range.length).toBe(2)
		})
		test("should do nothing with a length 0", () => {
			const range = new RichTextRange(2, 1)
			range.growRight(0)
			expect(range.offset).toBe(2)
			expect(range.length).toBe(1)
		})
		test("should fail with a negative length", () => {
			const range = new RichTextRange(0, 1)
			expect(() => {
				range.growRight(-1)
			}).toThrow()
		})
	})
	describe("moveLeft", () => {
		test("should succeed", () => {
			const range = new RichTextRange(2, 1)
			range.moveLeft(1)
			expect(range.offset).toBe(1)
			expect(range.length).toBe(1)
		})
		test("should do nothing with a length 0", () => {
			const range = new RichTextRange(2, 1)
			range.moveLeft(0)
			expect(range.offset).toBe(2)
			expect(range.length).toBe(1)
		})
		test("should fail with a negative length", () => {
			const range = new RichTextRange(0, 1)
			expect(() => {
				range.moveLeft(-1)
			}).toThrow()
		})
		test("should fail with a range with offset 0", () => {
			const range = new RichTextRange(0, 1)
			expect(() => {
				range.moveLeft(1)
			}).toThrow()
		})
	})
	describe("moveRight", () => {
		test("should succeed", () => {
			const range = new RichTextRange(2, 1)
			range.moveRight(1)
			expect(range.offset).toBe(3)
			expect(range.length).toBe(1)
		})
		test("should do nothing with a length 0", () => {
			const range = new RichTextRange(2, 1)
			range.moveRight(0)
			expect(range.offset).toBe(2)
			expect(range.length).toBe(1)
		})
		test("should fail with a negative length", () => {
			const range = new RichTextRange(0, 1)
			expect(() => {
				range.moveRight(-1)
			}).toThrow()
		})
	})
})

describe("deleteInRange", () => {
	test("should do nothing when selection is after range, not touching it", () => {
		const range = new RichTextRange(0, 5)
		const selection = new RichTextRange(6, 2)
		const range2 = deleteInRange(selection, range)
		expect(range2.offset).toBe(0)
		expect(range2.length).toBe(5)
	})
	test("should do nothing when selection is after range, touching it", () => {
		const range = new RichTextRange(0, 5)
		const selection = new RichTextRange(5, 2)
		const range2 = deleteInRange(selection, range)
		expect(range2.offset).toBe(0)
		expect(range2.length).toBe(5)
	})
	test("should do nothing when selection is collapsed", () => {
		const range = new RichTextRange(0, 5)
		const selection = new RichTextRange(2)
		const range2 = deleteInRange(selection, range)
		expect(range2.offset).toBe(0)
		expect(range2.length).toBe(5)
	})
	test("should fail when selection contains range", () => {
		const range = new RichTextRange(1, 5)
		const selection = new RichTextRange(0, 7)
		expect(() => {
			deleteInRange(selection, range)
		}).toThrow()
	})
	test("should fail when selection contains range exactly", () => {
		const range = new RichTextRange(1, 5)
		const selection = new RichTextRange(1, 5)
		expect(() => {
			deleteInRange(selection, range)
		}).toThrow()
	})
	test("should succeed when selection is before range", () => {
		const range = new RichTextRange(2, 5)
		const selection = new RichTextRange(0, 1)
		const range2 = deleteInRange(selection, range)
		expect(range2.offset).toBe(1)
		expect(range2.length).toBe(5)
	})
	test("should succeed when selection is inside range", () => {
		const range = new RichTextRange(2, 5)
		const selection = new RichTextRange(3, 1)
		const range2 = deleteInRange(selection, range)
		expect(range2.offset).toBe(2)
		expect(range2.length).toBe(4)
	})
	test("should succeed when selection intersects range", () => {
		const range = new RichTextRange(2, 5)
		const selection = new RichTextRange(1, 2)
		const range2 = deleteInRange(selection, range)
		expect(range2.offset).toBe(1)
		expect(range2.length).toBe(4)
	})
})

describe("insertInRange", () => {
	test("should succeed when caret is before range, not touching", () => {
		const range = new RichTextRange(2, 5)
		const range2 = insertInRange(0, 1, range)
		expect(range2.offset).toBe(3)
		expect(range2.length).toBe(5)
	})
	test("should succeed when caret is before range, touching range left", () => {
		const range = new RichTextRange(2, 5)
		const range2 = insertInRange(2, 1, range)
		expect(range2.offset).toBe(3)
		expect(range2.length).toBe(5)
	})
	test("should succeed when caret is inside range, not touching", () => {
		const range = new RichTextRange(2, 5)
		const range2 = insertInRange(3, 1, range)
		expect(range2.offset).toBe(2)
		expect(range2.length).toBe(6)
	})
	test("should succeed when caret is inside range, touching range right", () => {
		const range = new RichTextRange(2, 5)
		const range2 = insertInRange(5, 1, range)
		expect(range2.offset).toBe(2)
		expect(range2.length).toBe(6)
	})
	test("should do nothing when caret is after range", () => {
		const range = new RichTextRange(2, 5)
		const range2 = insertInRange(8, 1, range)
		expect(range2.offset).toBe(2)
		expect(range2.length).toBe(5)
	})
})

function createState() {
	const state = RichTextState.fromJSON({
		text: "there's bold, italic and both!",
		inlineStyleRanges: [
			{ offset: 8, length: 4, style: "BOLD" },
			{ offset: 14, length: 6, style: "ITALIC" },
			{ offset: 25, length: 4, style: "BOLD" },
			{ offset: 25, length: 4, style: "ITALIC" },
		],
	})
	return state
}

function rangesToJSON(ranges: RichTextRange[]) {
	return ranges.map((range) => range.toJSON())
}

describe("RichTextState", () => {
	describe("deleteText", () => {
		test("should succeed when deleting text with a selection of first character", () => {
			const state = createState()
			state.deleteText(new RichTextRange(0, 1))
			expect(state.text).toBe("here's bold, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8 - 1, length: 4, style: "BOLD" },
				{ offset: 14 - 1, length: 6, style: "ITALIC" },
				{ offset: 25 - 1, length: 4, style: "BOLD" },
				{ offset: 25 - 1, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when deleting text with a selection of all characters", () => {
			const state = createState()
			state.deleteText(new RichTextRange(0, state.text.length))
			expect(state.text).toBe("")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([])
		})
		test("should succeed when deleting text with a selection of a whole inline range", () => {
			const state = createState()
			state.deleteText(new RichTextRange(20, 9))
			expect(state.text).toBe("there's bold, italic!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 4, style: "BOLD" },
				{ offset: 14, length: 6, style: "ITALIC" },
			])
		})
	})
	describe("deleteTextBackward", () => {
		test("should do nothing when deleting text with a collapsed selection at the beginning", () => {
			const state = createState()
			state.deleteTextBackward(new RichTextRange(0))
			expect(state.text).toBe("there's bold, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 4, style: "BOLD" },
				{ offset: 14, length: 6, style: "ITALIC" },
				{ offset: 25, length: 4, style: "BOLD" },
				{ offset: 25, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when deleting text with a collapsed selection at first character", () => {
			const state = createState()
			state.deleteTextBackward(new RichTextRange(1))
			expect(state.text).toBe("here's bold, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8 - 1, length: 4, style: "BOLD" },
				{ offset: 14 - 1, length: 6, style: "ITALIC" },
				{ offset: 25 - 1, length: 4, style: "BOLD" },
				{ offset: 25 - 1, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when deleting text with a collapsed selection at the end", () => {
			const state = createState()
			state.deleteTextBackward(new RichTextRange(30))
			expect(state.text).toBe("there's bold, italic and both")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 4, style: "BOLD" },
				{ offset: 14, length: 6, style: "ITALIC" },
				{ offset: 25, length: 4, style: "BOLD" },
				{ offset: 25, length: 4, style: "ITALIC" },
			])
		})
	})
	describe("deleteTextForward", () => {
		test("should do nothing when deleting text with a collapsed selection at the end", () => {
			const state = createState()
			state.deleteTextForward(new RichTextRange(30))
			expect(state.text).toBe("there's bold, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 4, style: "BOLD" },
				{ offset: 14, length: 6, style: "ITALIC" },
				{ offset: 25, length: 4, style: "BOLD" },
				{ offset: 25, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when deleting text with a collapsed selection at the beginning", () => {
			const state = createState()
			state.deleteTextForward(new RichTextRange(0))
			expect(state.text).toBe("here's bold, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8 - 1, length: 4, style: "BOLD" },
				{ offset: 14 - 1, length: 6, style: "ITALIC" },
				{ offset: 25 - 1, length: 4, style: "BOLD" },
				{ offset: 25 - 1, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when deleting text with a collapsed selection at second last character", () => {
			const state = createState()
			state.deleteTextForward(new RichTextRange(29))
			expect(state.text).toBe("there's bold, italic and both")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 4, style: "BOLD" },
				{ offset: 14, length: 6, style: "ITALIC" },
				{ offset: 25, length: 4, style: "BOLD" },
				{ offset: 25, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when deleting text with a collapsed selection at the beginning of a word with an inline range", () => {
			const state = createState()
			state.deleteTextForward(new RichTextRange(8))
			expect(state.text).toBe("there's old, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 3, style: "BOLD" },
				{ offset: 14 - 1, length: 6, style: "ITALIC" },
				{ offset: 25 - 1, length: 4, style: "BOLD" },
				{ offset: 25 - 1, length: 4, style: "ITALIC" },
			])
		})
	})
	describe("insertText", () => {
		test("should succeed when inserting text at the beginning", () => {
			const state = createState()
			state.insertText(new RichTextRange(0), "Oh, ")
			expect(state.text).toBe("Oh, there's bold, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8 + 4, length: 4, style: "BOLD" },
				{ offset: 14 + 4, length: 6, style: "ITALIC" },
				{ offset: 25 + 4, length: 4, style: "BOLD" },
				{ offset: 25 + 4, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when inserting text before an inline style", () => {
			const state = createState()
			state.insertText(new RichTextRange(24), "...")
			expect(state.text).toBe("there's bold, italic and... both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 4, style: "BOLD" },
				{ offset: 14, length: 6, style: "ITALIC" },
				{ offset: 25 + 3, length: 4, style: "BOLD" },
				{ offset: 25 + 3, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when inserting text at the end", () => {
			const state = createState()
			state.insertText(new RichTextRange(30), "!!")
			expect(state.text).toBe("there's bold, italic and both!!!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 4, style: "BOLD" },
				{ offset: 14, length: 6, style: "ITALIC" },
				{ offset: 25, length: 4, style: "BOLD" },
				{ offset: 25, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when inserting text at the beginning of a word having an inline style", () => {
			const state = createState()
			state.insertText(new RichTextRange(8), "bb")
			expect(state.text).toBe("there's bbbold, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8 + 2, length: 4, style: "BOLD" },
				{ offset: 14 + 2, length: 6, style: "ITALIC" },
				{ offset: 25 + 2, length: 4, style: "BOLD" },
				{ offset: 25 + 2, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when inserting text in the middle of a world having an inline style", () => {
			const state = createState()
			state.insertText(new RichTextRange(10), "oo")
			expect(state.text).toBe("there's booold, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 4 + 2, style: "BOLD" },
				{ offset: 14 + 2, length: 6, style: "ITALIC" },
				{ offset: 25 + 2, length: 4, style: "BOLD" },
				{ offset: 25 + 2, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when inserting text at the end of a world having an inline style", () => {
			const state = createState()
			state.insertText(new RichTextRange(12), "dd")
			expect(state.text).toBe("there's bolddd, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 4 + 2, style: "BOLD" },
				{ offset: 14 + 2, length: 6, style: "ITALIC" },
				{ offset: 25 + 2, length: 4, style: "BOLD" },
				{ offset: 25 + 2, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when replacing text at the beginning of a word having an inline style", () => {
			const state = createState()
			state.insertText(new RichTextRange(8, 2), "xx")
			expect(state.text).toBe("there's xxld, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 10, length: 2, style: "BOLD" },
				{ offset: 14, length: 6, style: "ITALIC" },
				{ offset: 25, length: 4, style: "BOLD" },
				{ offset: 25, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when replacing text in the middle of a world having an inline style", () => {
			const state = createState()
			state.insertText(new RichTextRange(9, 1), "a")
			expect(state.text).toBe("there's bald, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 4, style: "BOLD" },
				{ offset: 14, length: 6, style: "ITALIC" },
				{ offset: 25, length: 4, style: "BOLD" },
				{ offset: 25, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when replacing text at the end of a world having an inline style", () => {
			const state = createState()
			state.insertText(new RichTextRange(10, 2), "o")
			expect(state.text).toBe("there's boo, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 3, style: "BOLD" },
				{ offset: 14 - 1, length: 6, style: "ITALIC" },
				{ offset: 25 - 1, length: 4, style: "BOLD" },
				{ offset: 25 - 1, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when replacing text before an inline style", () => {
			const state = createState()
			state.insertText(new RichTextRange(5, 2), " is")
			expect(state.text).toBe("there is bold, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 9, length: 4, style: "BOLD" },
				{ offset: 15, length: 6, style: "ITALIC" },
				{ offset: 26, length: 4, style: "BOLD" },
				{ offset: 26, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when replacing text with selection overlapping an inline style from left", () => {
			const state = createState()
			state.insertText(new RichTextRange(5, 5), " ")
			expect(state.text).toBe("there ld, italic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 6, length: 2, style: "BOLD" },
				{ offset: 10, length: 6, style: "ITALIC" },
				{ offset: 21, length: 4, style: "BOLD" },
				{ offset: 21, length: 4, style: "ITALIC" },
			])
		})
		test("should succeed when replacing text with selection overlapping an inline style from right", () => {
			const state = createState()
			state.insertText(new RichTextRange(10, 4), "o")
			expect(state.text).toBe("there's booitalic and both!")
			expect(rangesToJSON(state.inlineStyleRanges)).toEqual([
				{ offset: 8, length: 3, style: "BOLD" },
				{ offset: 11, length: 6, style: "ITALIC" },
				{ offset: 22, length: 4, style: "BOLD" },
				{ offset: 22, length: 4, style: "ITALIC" },
			])
		})
	})
})
